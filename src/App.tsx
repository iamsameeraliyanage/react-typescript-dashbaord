import React from "react";
import "./App.scss";
import LoginForm from "./components/Auth/LoginForm/LoginForm";
import SignupForm from "./components/Auth/SignupForm/SignupForm";

function App() {
  return (
    <div className="App">
      <LoginForm />
    </div>
  );
}

export default App;
